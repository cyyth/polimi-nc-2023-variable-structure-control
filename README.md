# Variable Structure Control (Sliding Mode Control)
One of the numerical simulation classes from Nonlinear Control 2023/2024 (instructed by Prof. Maria Prandini).

# Included Contents
### Live Scripts:
- [vsc_control.mlx](vsc_control.mlx)

### Simulink Models:
- [vsc_demo.slx](vsc_demo.slx)
![alt text](figures/vsc_demo.jpeg)

- [vsc_w_obs.slx](vsc_w_obs.slx)
![alt text](figures/vsc_w_obs.jpeg)

